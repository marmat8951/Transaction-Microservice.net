using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Transaction_Microservice.net.Models;
using System.Linq;

namespace Transaction_Microservice.net.Controllers
{
    [Route("api/[controller]")] //Set the main URL access to /api/account
    public class AccountController : Controller
    {
        private readonly AccountContext _context;  //Context we are using to store and retrieve data.

        public AccountController(AccountContext context)
        {
            _context = context;
            if (_context.Account.Count() == 0) //If there isn't any account at startup, so, initialise the database
            {
                _context.SaveChanges();
            }
        }


        /**
        Method used to get a result when a GET is used on /api/account
        this produces Application/JSON type of data.
        This return the whole AccountList.
        
         */
        [HttpGet]   
        public IEnumerable<Account> GetAll()
        {
            return _context.Account.ToList();
        }

        /**
        Method used when a GET is used with the URL /api/account/{id}
        the id param is an long representing the accountID requested.
        It produces an Application/JSON content-type.
         */
        [HttpGet("{id}", Name = "GetAccount")]
        public IActionResult GetById(long id)
        {
            var item = _context.Account.FirstOrDefault(t => t.Id == id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        /**
        Called when GET is used on api/account/pid/{pid}
        the pid param is an integer representing the ProfileID for which we want the account(s)
        It produces an Application/JSON content-type as an array containing all the accounts corresponding
         */
        [HttpGet("pid/{pid}", Name = "GetAllAccountByPid")]
        public IActionResult GetByPid(int pid)
        {
            var all = GetAll();
            List<Account> liste = new List<Account>();
            foreach (Account a in all)
            {
                if (a.pid == pid)
                {
                    liste.Add(a);
                }
            }
            return new ObjectResult(liste);

        }


        /**
        Called when a POST is used on api/account/deposit/{id}
        the id is an long representing the account ID we are crediting.
        This Consumes Plain-text Representing the ammount we want to credit to the account formatted as float.
        If number is <= 0 or if the number couldn't be parsed, it return a 400 Bad Request
         */
        [HttpPost("deposit/{id}", Name = "Deposit")]
        public IActionResult DepositOnId(long id, [FromBody] string amount)
        {
            var item = _context.Account.FirstOrDefault(t => t.Id == id);
            float a = float.Parse(amount);
            if (a <= 0)
            {
                return BadRequest();
            }
            if (item == null)
            {
                return NotFound();
            }
            else
            {
                item.Balance = item.Balance + a;
                _context.SaveChanges();
                return NoContent();
            }

        }

        /**
        Called when a POST is used on api/account/withdraw/{id}
        the id is an long representing the account ID we are debitting.
        This Consumes Plain-text Representing the ammount as a number we want remove from the account formated as float
        If number is <= 0 or if the number couldn't be parsed, it return a 400 Bad Request 
         */
        [HttpPost("withdraw/{id}", Name = "Withdraw")]
        public IActionResult Withdraw(long id, [FromBody] string amount)
        {
            var item = _context.Account.FirstOrDefault(t => t.Id == id);
            float a = float.Parse(amount);
            if (a <= 0)
            {
                return BadRequest();
            }
            if (item == null)
            {
                return NotFound();
            }
            else
            {
                item.Balance = item.Balance - a;
                _context.SaveChanges();
                return NoContent();
            }

        }

        /**
        Called when a Post appends on /api/account
        This leads to create a new account
        It consumes application/Json data representing an account
         */
        [HttpPost]
        public IActionResult Create([FromBody] Account account)
        {
            if (account == null)
            {
                return BadRequest();
            }
            _context.Account.Add(account);
            _context.SaveChanges();

            return CreatedAtRoute("getAccount", new { id = account.Id }, account);
        }

        /**
        Called when a DELETE appends on /api/account/{id}
        id is a number that represent the id of the account we want to delete
        This leads to delete the account with the id {id}
        If it works, then return an No Content Response.
         */
        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var account = _context.Account.FirstOrDefault(t => t.Id == id);
            if (account == null)
            {
                return NotFound();
            }

            _context.Account.Remove(account);
            _context.SaveChanges();
            return new NoContentResult();
        }
        /**
        Called when a DELETE appends on /api/account/pid/{pid}
        pid is a number that represent the pid of the account(s) we wat to delete
        This leads to delete the account with the id {id}
        If it works, then return an No Content Response.
         */
        [HttpDelete("pid/{id}")]
        public IActionResult DeleteUsingPid(int pid)
        {
            var account = GetAll();
            foreach (Account a in account)
            {
                if (a.pid == pid)
                {
                    _context.Account.Remove(a);
                }
            }
            _context.SaveChanges();
            return new NoContentResult();
        }

        /**
        Called when a PUT appends on /api/account/{id}
        id is an integer representing the Accountid we are modifing.
        It checks if the id corresponds beetween the Body one and the Path one
        it consumes Application/JSON data from the body.
        and then it replace the previous datas with the new representation
        
         */
        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] Account account)
        {
            if (account == null || account.Id != id)
            {
                return BadRequest();
            }

            var ac = _context.Account.FirstOrDefault(t => t.Id == id);
            if (ac == null)
            {
                return NotFound();
            }

            ac.type = account.type;
            ac.Balance = account.Balance;

            _context.Account.Update(ac);
            _context.SaveChanges();
            return new NoContentResult();
        }

        /**
        Called when a Get is made on /api/account/sum
        It makes the sum of all accounts in the database and print it as text/plaintext
        representing a float number
         */

        [HttpGet("sum")]
        public IActionResult GetSum()
        {
            Account[] array = _context.Account.ToArray();
            double sum = 0;
            for (int i = 0; i < array.Length; ++i)
            {
                sum += array[i].Balance;
            }
            return new ObjectResult(sum);
        }

    }
}