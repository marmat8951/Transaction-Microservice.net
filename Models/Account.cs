namespace Transaction_Microservice.net.Models
/**
    This class is the model with represent an Account in the Database and
    as an object to work with while inserting or deleting things.

  */
{
    public class Account
    {
        public long Id { get; set; }
        public float Balance { get; set; }
        public string type { get; set; }
        public int pid {get; set; } //This represents the pid this accound is linked to
    }
}