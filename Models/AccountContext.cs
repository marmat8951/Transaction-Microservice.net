using Microsoft.EntityFrameworkCore;

namespace Transaction_Microservice.net.Models
/**
    This is the context. This serve ti create the database for the Account in memory
 */

{
    public class AccountContext : DbContext
    {
        public AccountContext(DbContextOptions<AccountContext> options)
            : base(options)
        {
        }

        public DbSet<Account> Account { get; set; }

    }
}