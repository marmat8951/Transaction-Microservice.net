
FROM microsoft/aspnetcore-build:latest
LABEL Name=transaction-microservice.net Version=0.0.1
ARG source=.
WORKDIR /app
EXPOSE 5000/tcp
ENV ASPNETCORE_URLS http://*:5000
COPY $source ./
CMD [ "dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Tools --version 2.0.0" ]
ENTRYPOINT ["dotnet", "Transaction-Microservice.net.dll"]
