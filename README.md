## MicroService REST Bank Account Management in .net

This microService has been developped as an exercice in a intership for the Salaberry de ValleyField CEGEP.

It can be used to create, store bank accounts, withdraw and deposit

You can as well simply pull and run [this docker](https://hub.docker.com/r/marmat8951/transac-microapp.net/)
### Requirements:

dotnet

### How to run:

First you need to clone or download this repository.

Then in the folder containing this piece of software, run thoses command:

``` Bash
dotnet restore
dotnet build
cd bin/Debug/netcoreapp2.0
dotnet Transaction-Microservice.net.dll

```
This will lunch the application on your computer, this API is listening on port **5000**

### Ressource Objects:

You will manipulate mostly only one object (the account), each time using Application/JSON content type.

**Account :** 
{
"balance":float,
"id":long,
"pid":int,
"type":String
}

Note: The String type is a temporary placeholder for the AccountType Class, and might be change in the future.

### API calls

#### HTTP GET

*/api/account*  
Return all the accounts in the database as an JSON array

*/api/account/{id:[0-9]+}*  
Return the account with the ID {id} passed as a PathParam and return it as Account JSON Object.

*/api/account/sum*  
Return a String Containg the total amount of money in the accounts.

*/api/account/test*  
Return a Test Account JSON object.

*/api/account/pid/{pid: [0-9]+}*
Return an Array of accounts that belong to the PersonId pid.

#### HTTP POST

*/api/account*  
Create a new Account using the Account JSON object passed in the body of the POST request. Failes if the Account ID already exists.

*/api/account/deposit/{id:[0-9]+}*  
Make a deposit on the account passed in parameter.
The deposit amount must be send as a Text/plain float formated  in the request body. The amount can't be <= 0.

*/api/account/withdraw/{id:[0-9]+}*  
Make a withdraw on the account passed in parameter.
The withdraw amount must be send as a Text/plain float formated  in the request body. The amount can't be <= 0.

#### HTTP PUT

Put must be used to modify an account.
Must have in the body the new JSON object Account.
the id in the Account object and in the path must be the same.

*/api/account/{id: [0-9]+}*  
Modify the Account with id {id}

#### HTTP DELETE

Delete is used to eather Delete an account or Delete all accounts corresponding to a person.

*/api/account/{id: [0-9]+}*  
Delete the account with the id {id}

*/api/account/pid/{pid: [0-9]+}*  
Delete all accounts that belong to the Person with the Pid pid
